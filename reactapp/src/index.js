import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
// import { Button } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

//pages
import Navbar from './partials/Navbar' //kahit walang extension kasi js din sya.
import MembersPage from './pages/MembersPage'



const root = document.querySelector("#root")

// const pageComponent = (
// 	<Fragment>
// 	<h1 className="header"> Hello, Batch 40! </h1>
// 	<button className="button"> Pink </button>
// 	<button className="btn btn-danger"> Danger </button>

// 	<Button color="primary" className="ml-3"> Reactstrap Button </Button>

// {/* how to comment out*/}
// 	</Fragment>
// 	)

const pageComponent = (
	<Fragment>
		<Navbar/>
		<MembersPage/>
	</Fragment>)

ReactDOM.render(pageComponent, root);