import React from 'react';
import { Table } from 'reactstrap';

const Example = (props) => {
  return (
        <tr>
          <th scope="row">1</th>
          <td>Bernz</td>
          <td>email@email.com</td>
          <td>Team Hayahay</td>
          <td>Admin</td>
          <td>
            <button className="btn btn-primary ml-1"><i class="far fa-eye"></i></button>
            <button className="btn btn-warning ml-1"><i class="far fa-edit"></i></button>
            <button className="btn btn-danger ml-1"><i class="far fa-trash-alt"></i></button>
          </td>
        </tr> 
  );
}

export default Example;