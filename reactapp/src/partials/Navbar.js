import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

const Example = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className = "mb-5">
      <Navbar color="light" light expand="md" className="bg-dark">
        <NavbarBrand className="font-weight-bold text-white" href="/">MERN Tracker</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink href="" className="font-weight-bold">Members</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="" className="font-weight-bold">Teams</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="" className="font-weight-bold">Tasks</NavLink>
            </NavItem>
          </Nav>
          <button className="btn btn-primary font-weight-bold">Login</button>
          <button className="btn btn-outlined text-white ml-1 font-weight-bold">Profile</button>
          <button className="btn btn-secondary ml-1 font-weight-bold">Logout</button>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default Example;