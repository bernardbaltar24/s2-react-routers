import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import MemberForm from '../forms/MemberForm';
import MemberTableHead from '../tables/MemberTableHead';

const Example = (props) => {
  return (
    <Container>
      <Row className="mb-5">
        <Col>
        	<h1>Members Page</h1>
        </Col>
      </Row>
      <Row className="d-flex">
        <Col md="4" className="border">
        	<MemberForm/>
        </Col>
        <Col md="8" className="">
        	<MemberTableHead/>
        </Col>
      </Row>
    </Container>
  );
}

export default Example;